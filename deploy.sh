#!/usr/bin/env bash
## author: reillo <atilloregner@gmail.com>
## description: purpose of this is to update the live server with belmont packages.

## configuration
remoteUser=accj
targetPath=/home/accj/public_html
repo=origin
branch=master
port=9384
domain=accj.lmweb.com.au
merging="reset --hard" ### "merge" or "reset --hard"

## deployer information
vhostname=$(hostname)
vuser=$USER

# push current git master to repo
git push origin ${branch}

ssh -p ${port} ${remoteUser}@${domain} /bin/bash << EOF
  cd ${targetPath} || exit

  # fecth repo
  umask 022
  unset GIT_DIR
  git fetch ${repo}
  git stash save "stash-backup-\$(date +"%Y-%m-%d")"
  git ${merging} ${repo}/${branch}

  # log last deploy
  touch deployment_log
  logContent="${vuser}@${vhostname} : Last Deployed \$(date +"%Y-%m-%d")"
  echo \${logContent} >> "deployment_log"
EOF
