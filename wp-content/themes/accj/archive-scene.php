<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
    <div class="section section-default">
        <div class="container">
            <div class="alert alert-warning">
                <?php _e('Sorry, no results were found.', 'sage'); ?>
            </div>
        </div>
        <div class="container">
            <?php get_search_form(); ?>
        </div>
    </div>
<?php else: ?>
    <?php get_template_part('templates/archive','intro'); ?>
    <div class="section section-default">
        <div class="container">
            <div class="card-deck card-deck-pdf">
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <?php endwhile; ?>
            </div>
            <div class="paginated-links">
                <?php custom_paginate_links(); ?>
            </div>

        </div>
    </div>
<?php endif; ?>