<?php get_template_part('templates/page', 'header'); ?>
<?php if (!have_posts()) : ?>
    <div class="section section-default">
        <div class="container">
            <div class="alert alert-warning">
                <?php _e('Sorry, no results were found.', 'sage'); ?>
            </div>
        </div>
        <div class="container">
            <?php get_search_form(); ?>
        </div>
    </div>
<?php else: ?>
    <?php get_template_part('templates/archive','intro'); ?>
    <div class="section section-default section-with-sidebar">
        <div class="container">
            <div class="section-news-media-list">
                <div class="row row-resources">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                    <?php endwhile; ?>
                </div>
                <?php if (custom_paginate_links()):?>
                    <div class="paginated-links">
                        <?php custom_paginate_links(); ?>
                    </div>
                <?php endif;?>
            </div>
            <?php include locate_template('templates/archive-values.php')?>
            <?php if ($archive_type): ?>
                <?php foreach ($archive_type as $type): ?>
                    <?php if ($type['cpt_name'] == $post_type_slug): ?>
                        <?php if (!empty($type['sidebar_title']) || $type['sidebar']): ?>
                            <div class="sidebar">
                                <div class="widget">
                                    <?php if (!empty($type['sidebar_title'])): ?>
                                        <h3><?php echo $type['sidebar_title']?></h3>
                                    <?php endif;?>
                                    <?php if (!empty($sidebar = $type['sidebar'])):?>
                                        <ul>
                                            <?php foreach ($sidebar as $item):?>
                                                <li><a href="<?php echo $item['link']['url']; ?>" <?php echo !empty($item['link']['target'])?'target="'.$item['link']['target'].'"':''; ?>><?php echo $item['link']['title']; ?></a></li>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>