<?php define('assest_version_css','5.1');?>
<?php define('assest_version_js','4.9');?>
<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',    // Scripts and stylesheets
    'lib/extras.php',    // Custom functions
    'lib/setup.php',     // Theme setup
    'lib/titles.php',    // Page titles
    'lib/wrapper.php',   // Theme wrapper class
    'lib/customizer.php', // Theme customizer
    'lib/navwalker-bs.php', // Navwalker BS
    'lib/breadcrumb.php' // Breadcrumbs
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

/*
Sample...  Lorem ipsum habitant morbi (26 characters total)

Returns first three words which is exactly 21 characters including spaces
Example..  echo get_excerpt(21);
Result...  Lorem ipsum habitant

Returns same as above, not enough characters in limit to return last word
Example..  echo get_excerpt(24);
Result...  Lorem ipsum habitant

Returns all 26 chars of our content, 30 char limit given, only 26 characters needed.
Example..  echo get_excerpt(30);
Result...  Lorem ipsum habitant morbi
*/
function get_excerpt($limit, $source = null){

    $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    return $excerpt;
}


function custom_post_type_resources_query( $query ){
    if(! is_admin() && $query->is_post_type_archive( 'resources' ) && $query->is_main_query() ){
        $query->set( 'posts_per_page', 6 );
    }
    elseif(! is_admin() && $query->is_post_type_archive( 'events' ) && $query->is_main_query() ){
        $query->set( 'posts_per_page', 9 );
    }
    elseif(! is_admin() && $query->is_post_type_archive( 'scene' ) && $query->is_main_query() ){
        $query->set( 'posts_per_page', 9 );
    }
}
add_action( 'pre_get_posts', 'custom_post_type_resources_query' );



function custom_paginate_links() {
    echo $pages = paginate_links( array(
        'prev_text'          => __('<i class="icon-rewind"></i>'),
        'next_text'          => __('<i class="icon-arrow"></i>')
    ) );

}





/**
 * Google recaptcha add before the submit button
 */
function add_google_recaptcha($submit_field) {
    $submit_field['submit_field'] = '<div class="g-recaptcha" data-sitekey="6LfEoHAUAAAAAPx55jpDNu8H5B-m6sBcI6DgRkeD"></div><br>' . $submit_field['submit_field'];
    return $submit_field;
}
if (!is_user_logged_in()) {
    add_filter('comment_form_defaults','add_google_recaptcha');
}

/**
 * Google recaptcha check, validate and catch the spammer
 */
function is_valid_captcha($captcha) {
    $captcha_postdata = http_build_query(array(
        'secret' => '6LfEoHAUAAAAAL21NkFmuDkC3bsNAELAg7JDmcdZ',
        'response' => $captcha,
        'remoteip' => $_SERVER['REMOTE_ADDR']));
    $captcha_opts = array('http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $captcha_postdata));
    $captcha_context  = stream_context_create($captcha_opts);
    $captcha_response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify" , false , $captcha_context), true);
    if ($captcha_response['success'])
        return true;
    else
        return false;
}

function verify_google_recaptcha() {
    $recaptcha = $_POST['g-recaptcha-response'];
    if (empty($recaptcha))
        wp_die( __("<b>ERROR:</b> please select <b>I'm not a robot!</b><p><a href='javascript:history.back()'>« Back</a></p>"));
    else if (!is_valid_captcha($recaptcha))
        wp_die( __("<b>Go away SPAMMER!</b>"));
}
if (!is_user_logged_in()) {
    add_action('pre_comment_on_post', 'verify_google_recaptcha');
}



function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}


add_action('admin_head', 'remove_content_editor');
/**
 * Remove the content editor from ALL pages
 */
function remove_content_editor()
{
    remove_post_type_support('page', 'editor');
}


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    acf_add_options_sub_page('Option');
}


add_theme_support( 'custom-logo' );
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 67,
        'width'       => 386,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );


function replace_dashes($string) {
    $string = strtolower(str_replace(" ", "-", $string));
    return $string;
}

add_theme_support( 'post-thumbnails' );
add_image_size( 'pdf_thumb', 357, 504 ); // Soft Crop
add_image_size( 'event_thumbnails', 348, 225, true ); // Hard Crop


function wpse_172645_get_post_types_by_taxonomy( $tax = 'category' )
{
    global $wp_taxonomies;
    return ( isset( $wp_taxonomies[$tax] ) ) ? $wp_taxonomies[$tax]->object_type : array();
}