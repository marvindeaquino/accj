<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>
<?php while (have_posts()) : the_post(); ?>
    <div class="section-post-single">
        <article <?php post_class(); ?>>
            <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <div class="meta-blog">
                    <?php get_template_part('templates/entry-meta'); ?>
                    <p class="ml-auto">Share</p>
                    <?php echo do_shortcode('[fny id="1"]')?>
                </div>

            </header>
            <div class="entry-content">
                <?php the_content(); ?>
                <?php tribe_get_template_part( 'modules/meta' ); ?>
            </div>
            <?php comments_template('/templates/comments.php'); ?>
        </article>
        <aside class="sidebar">
            <?php dynamic_sidebar('sidebar-news-media'); ?>
        </aside>
    </div>
<?php endwhile; ?>
