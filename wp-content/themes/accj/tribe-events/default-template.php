<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>
<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/archive','intro'); ?>
<div id="tribe-events-pg-template" class="tribe-events-pg-template">
    <div class="container">
        <?php tribe_events_before_html(); ?>
        <?php tribe_get_view(); ?>
        <?php tribe_events_after_html(); ?>
    </div>
</div> <!-- #tribe-events-pg-template -->
<?php

