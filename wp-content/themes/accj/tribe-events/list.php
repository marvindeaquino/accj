<?php
/**
 * List View Template
 * The wrapper template for a list of events. This includes the Past Events and Upcoming Events views
 * as well as those same views filtered to a specific category.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

do_action( 'tribe_events_before_template' );
?>
    <div class="section section-default">
        <div class="container">
            <?php /*get_template_part('templates/search','filter'); */?>
            <?php if (!have_posts()) : ?>
                <div class="alert alert-warning">
                    <?php _e('Sorry, no available events were found.', 'sage'); ?>
                </div>
                <?php get_search_form(); ?>
            <?php else: ?>
                <div class="card-deck card-deck-news-media">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('templates/content', 'events'); ?>
                    <?php endwhile; ?>
                </div>
                <div class="paginated-links">
                    <?php custom_paginate_links(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php
do_action( 'tribe_events_after_template' );
