<div class="section section-default section-404" style="background: transparent url('<?php echo get_template_directory_uri()?>/dist/images/home/home-banner-org.jpg') no-repeat center center; background-size: cover;">
    <div class="container">
        <h1>404</h1>
        <h2>Page Not Found</h2>
        <div>
            <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
        </div>
    </div>

</div>

