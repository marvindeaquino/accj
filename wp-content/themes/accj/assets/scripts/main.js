/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
          $( "span.page-numbers.dots" ).replaceWith( "<span class='page-numbers dots'><i class='icon-three-dots-more-indicator'></i></span>" );
          $('.search-trigger').click(function (e) {
              e.stopPropagation();
              $('.blog-filter').toggleClass('search-triggered');
              $('.search-field').focus();
          });
          $('.navbar-category .navbar-toggler.hamburger').click(function (e) {
              e.stopPropagation();
              $('.blog-filter').toggleClass('blog-filter-expand');
          });
          $('.navbar-toggler.hamburger').click(function() {
              $('.search-trigger').toggle();
          });

          // Scroll to the desired section on click
          // Make sure to add the `data-scroll` attribute to your `<a>`          tag.
          // Example:
          // `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.

          function scrollToSection(event) {
              if ( window.location.pathname == '/' ){
                  event.preventDefault();
                  var $section = $($(this).attr('href'));
                  $('html, body').animate({
                      scrollTop: $section.offset().top-70
                  }, 500);
              }
          }
          $('[data-scroll]').on('click', scrollToSection);


          function scrollToSection2(event) {
              if ( window.location.pathname == '/' ){
                  event.preventDefault();
                  var $section = $($(this).attr('href'));
                  $('html, body').animate({
                      scrollTop: $section.offset().top-100
                  }, 500);
              }
          }
          $('[data-scroll2]').on('click', scrollToSection2);


          /**
           * forEach implementation for Objects/NodeLists/Arrays, automatic type loops and context options
           *
           * @private
           * @author Todd Motto
           * @link https://github.com/toddmotto/foreach
           * @param {Array|Object|NodeList} collection - Collection of items to iterate, could be an Array, Object or NodeList
           * @callback requestCallback      callback   - Callback function for each iteration.
           * @param {Array|Object|NodeList} scope=null - Object/NodeList/Array that forEach is iterating over, to use as the this value when executing callback.
           * @returns {}
           */
          var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

          var hamburgers = document.querySelectorAll(".hamburger");
          if (hamburgers.length > 0) {
              forEach(hamburgers, function(hamburger) {
                  hamburger.addEventListener("click", function() {
                      this.classList.toggle("is-active");
                  }, false);
              });
          }
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired

      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'blog': {
        init: function() {
            // JavaScript to be fired on the about us page

        }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
