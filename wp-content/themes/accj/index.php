<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/archive','intro'); ?>
<div class="section section-default section-news-media-list">
    <div class="container">
        <?php if (is_blog() || !is_post_type_archive('news')): ?>
            <?php include(locate_template('templates/search-filter.php')); ?>
        <?php endif; ?>

        <?php if (!have_posts()) : ?>
            <div class="alert alert-warning">
                <?php _e('Sorry, no results were found.', 'sage'); ?>
            </div>
            <?php get_search_form(); ?>
        <?php else: ?>

            <div class="card-deck card-deck-news-media">
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <?php endwhile; ?>
            </div>
            <div class="paginated-links">
                <?php custom_paginate_links(); ?>
            </div>
        <?php endif; ?>
    </div>
</div>