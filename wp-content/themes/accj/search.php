<?php get_template_part('templates/page', 'header'); ?>
<div class="section section-default">
    <div class="container">
        <?php get_template_part('templates/search', 'filter'); ?>
    </div>
    <?php if (!have_posts()) : ?>
        <div class="container">
            <div class="alert alert-primary">
                <?php _e('Sorry, no results were found.', 'sage'); ?>
            </div>
        </div>

        <?php get_search_form(); ?>
    <?php endif; ?>
    <div class="container">
        <div class="card-deck card-deck-news-media">
            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/content', ''); ?>
            <?php endwhile; ?>
        </div>
        <div class="paginated-links">
            <?php custom_paginate_links(); ?>
        </div>
    </div>
</div>

<?php the_posts_navigation(); ?>
