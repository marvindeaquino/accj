<?php $card_tiles = get_sub_field('card_tiles'); ?>
<div class="section-card-tiles section-default">
    <div class="container">
        <?php echo $card_tiles['content'];?>
        <?php if ($card_items = $card_tiles['card_item']): ?>
            <div class="row">
                <?php foreach ($card_items as $card_item):?>
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <img src="<?php echo get_template_directory_uri()?>/dist/images/placeholder.png" alt="" class="card-icon">
                                <?php echo $card_item['content'];?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
    </div>
</div>