<?php include locate_template('templates/archive-values.php')?>
<?php if ($archive_type): ?>
    <?php foreach ($archive_type as $type): ?>
        <?php if ($type['cpt_name'] == $post_type_slug): ?>
            <?php if (!empty($type['content'])): ?>
                <div class="section-archive-intro">
                    <div class="container">
                        <div class="hero-intro">
                            <?php echo $type['content']; ?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        <?php endif;?>
    <?php endforeach;?>
<?php endif; ?>