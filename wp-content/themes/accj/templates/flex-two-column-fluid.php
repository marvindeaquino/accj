<?php $two_column_block_fluids = get_sub_field('two_column_block_fluid'); ?>
<!--Community Section-->
<div class="section section-community">
    <div class="community-container">
        <?php if ($two_column_block_fluids): ?>
            <?php foreach ($two_column_block_fluids as $two_column_block_fluid):?>
                <div class="community-row">
                    <div class="community-img">
                        <img src="<?php echo $two_column_block_fluid['twin_background_image']['url']; ?>" alt="<?php echo $two_column_block_fluid['twin_background_image']['alt']; ?>">
                    </div>
                    <div class="community-content" style="background: <?php echo $two_column_block_fluid['twin_one_column_block']['background_color_blend']; ?> url('<?php echo $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['background_image']['url']; ?>') no-repeat center;background-size: cover;">
                        <div class="content-details">
                            <?php echo $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['content']; ?>
                            <?php if (!empty($two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link'])): ?>
                                <a href="<?php echo $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link']['url']; ?>" <?php echo $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link']['target'] ? 'target="'.$two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link']['target'].'"':''; ?> class="btn <?php echo $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['button_type']['value']; ?>">
                                    <?php echo !empty($two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link']['title']) ? $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['link']['title'] : $two_column_block_fluid['twin_one_column_block']['hero_master_banner']['button_type']['label']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        <?php endif; ?>
    </div>
</div>
<!--End of Community Section-->