<div class="card">
    <?php if (!empty(get_the_post_thumbnail())):?>
        <?php the_post_thumbnail('pdf_thumb', array('class' => 'card-img-top')); ?>
    <?php else: ?>
        <img width="356" height="504" src="<?php echo get_template_directory_uri();?>/dist/images/subpage/pdf-dummy.png"  class="card-img-top" alt="">
    <?php endif;?>

    <div class="card-body">
        <h5 class="card-title"><?php the_title(); ?></h5>
    </div>
    <div class="card-footer">
        <?php $pdf = get_field('pdf');?>
        <a data-fancybox data-type="iframe" data-src="<?php echo $pdf['url']; ?>" href="javascript:;" class="btn btn-secondary btn-view-pdf">View</a>
        <a href="<?php echo $pdf['url']; ?>" class="btn btn-primary btn-download">Download</a>
    </div>
</div>