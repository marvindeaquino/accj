<div class="card">
    <?php if (!empty(get_the_post_thumbnail())):?>
        <!--<div class="card-img-holder">
            <?php /*the_post_thumbnail('full', array('class' => 'card-img-top')); */?>
        </div>-->
        <?php the_post_thumbnail('event_thumbnails', array('class' => 'card-img-top')); ?>
    <?php else: ?>
        <img width="281" height="225" src="<?php echo get_template_directory_uri();?>/dist/images/subpage/blog-img-0.jpg"  class="card-img-top" alt="">
    <?php endif;?>

    <div class="card-body">
        <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
        <?php get_template_part('templates/entry-meta-events'); ?>
        <?php echo get_excerpt(95); ?>
    </div>
    <div class="card-footer text-center">
        <a href="<?php the_permalink()?>" class="btn btn-link btn-read-more">Read more</a>
    </div>
</div>