<?php use Roots\Sage\Titles; ?>
<?php if (!is_front_page() && !is_single()): ?>
    <div class="page-header subpage-banner">
        <div class="container">
            <?php if (is_page()): ?>
                <h1><?= Titles\title(); ?></h1>
            <?php elseif (is_category() || is_search() || is_blog()):?>
                <h1><?php echo get_the_title( get_option('page_for_posts', true) ); ?></h1>
            <?php else: ?>
                <h1><?php echo post_type_archive_title();?></h1>
            <?php endif;?>
        </div>
    </div>
<?php elseif (is_single()):?>
    <div class="section-breadcrumb secondary-bg">
        <div class="container">
            <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
            <div><a href="<?php echo get_permalink( get_option( 'page_for_posts') ) ?>">News</a></div>
        </div>
    </div>
<?php endif;?>

