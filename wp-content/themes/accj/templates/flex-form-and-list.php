<?php $form_and_list = get_sub_field('form_and_list'); ?>
<!--Form and List -->
<div class="section section-default section-page-single">
    <div class="container">
        <div class="intro">
            <?php echo $form_and_list['content']; ?>
        </div>
        <div class="contact-content">
            <div class="contact-form">
                <?php echo $form_and_list['content_form']; ?>
            </div>
            <div class="contact-details">
                <address>
                    <?php echo $form_and_list['list']; ?>
                </address>
            </div>
        </div>
    </div>
</div>
<!--End of Form and List -->