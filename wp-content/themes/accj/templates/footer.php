<footer class="content-info footer-border-top">
  <div class="container">
      <nav class="navbar navbar-expand-xl navbar-light">
          <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
              <?php
                  $custom_logo_id = get_theme_mod( 'custom_logo' );
                  $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
              ?>
              <img class="img-fluid" src="<?php echo $image[0]; ?>" alt="">
          </a>
          <button class="navbar-toggler hamburger hamburger--slider" type="button" data-toggle="collapse" data-target="#navbarFooter" aria-controls="navbarFooter" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
          </button>
          <div class="collapse navbar-collapse ml-auto" id="navbarFooter">
              <?php
              if (has_nav_menu('footer_navigation')) :
                  wp_nav_menu([
                      'theme_location' => 'footer_navigation',
                      'container' => false,
                      'menu_class' => 'navbar-nav ml-auto',
                      'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                      'walker' => new WP_Bootstrap_Navwalker()
                  ]);
              endif;
              ?>
          </div>
      </nav>
      <div class="copyright">
          <?php the_field('copyright','option');?>
      </div>
  </div>
</footer>
