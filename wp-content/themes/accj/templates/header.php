<header class="navbar-header<?php echo (!is_front_page()) ? ' p-0':''; ?>">
    <div class="container">
        <nav class="navbar navbar-light nav-primary navbar-expand-xl">
            <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                <?php
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                ?>
                <img class="img-fluid" src="<?php echo $image[0]; ?>" alt="">
            </a>
            <button class="navbar-toggler hamburger hamburger--slider" type="button" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>

            <div class="collapse navbar-collapse" id="navbar-primary">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu([
                        'theme_location' => 'primary_navigation',
                        'container' => false,
                        'menu_class' => 'navbar-nav ml-auto',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker()
                    ]);
                endif;
                ?>
            </div>
        </nav>
    </div>
</header>
