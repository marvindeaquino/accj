<!--Master Home Banner-->
<?php $master_banner = get_sub_field('master_banner'); ?>
<div class="section section-master-banner" id="<?php echo $master_banner['section_id'];?>" style="background: transparent url('<?php echo $master_banner['background_image']['url'];?>') no-repeat center center; background-size: cover;">
    <div class="container">
        <div class="masterhead-content">
            <?php echo $master_banner['content']; ?>
            <?php if (!empty($master_banner['link'])): ?>
                <a href="<?php echo $master_banner['link']['url']?>" <?php echo $master_banner['link']['target'] ? 'target="'.$master_banner['link']['target'].'"':''; ?> class="btn btn-primary <?php echo $master_banner['button_type']['value']; ?>">
                    <?php echo !empty($master_banner['link']['title']) ? $master_banner['link']['title'] : $master_banner['button_type']['label']; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="bottom-anchor">
        <a data-scroll href="#who-we-are" data-bottomanchor="">
            <i class="icon-expand-button"></i>
        </a>
    </div>
</div>
<!--End of Master Home Banner-->


