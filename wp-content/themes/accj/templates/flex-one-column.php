<?php $one_column_block = get_sub_field('one_column_block'); ?>

<!--Section Intro-->
<div class="section section-default section-intro <?php echo !empty($one_column_block['background_color_blend'] && $one_column_block['hero_master_banner']['background_image']['url']) ? 'section-with-bg': ''; ?>" id="<?php echo replace_dashes($one_column_block['hero_master_banner']['section_id']); ?>" style="background: <?php echo !empty($one_column_block['background_color_blend']) ? $one_column_block['background_color_blend']: ''; ?> url('<?php echo (!empty($one_column_block['hero_master_banner']['background_image']['url'])) ? $one_column_block['hero_master_banner']['background_image']['url']:''; ?>') no-repeat center;background-size: cover;">
    <div class="container">
        <?php echo $one_column_block['hero_master_banner']['content']; ?>
        <?php if (!empty($one_column_block['hero_master_banner']['link'])): ?>
            <a href="<?php echo $one_column_block['hero_master_banner']['link']['url']; ?>" <?php echo $one_column_block['hero_master_banner']['link']['target'] ? 'target="'.$one_column_block['hero_master_banner']['link']['target'].'"':''; ?> class="btn btn-primary <?php echo $one_column_block['hero_master_banner']['button_type']['value']; ?>">
                <?php echo !empty($one_column_block['hero_master_banner']['link']['title']) ? $one_column_block['hero_master_banner']['link']['title'] : $one_column_block['hero_master_banner']['button_type']['label']; ?>
            </a>
        <?php endif; ?>
    </div>
</div>
<!--End of Section Intro-->
