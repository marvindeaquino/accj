<?php
///Get The Post Type of Current Taxonomy Page
$queried_object = get_queried_object();
$taxonomies = $queried_object->taxonomies;
$taxonomy = $queried_object->taxonomy;
$out = wpse_172645_get_post_types_by_taxonomy( $taxonomy );
$archive_link = get_post_type_archive_link($out[0]);
?>
<?php
    if(is_post_type_archive('news')){
        $args = array(
            'taxonomy' => $taxonomies[0],
            'hide_empty' => 0,
        );
    }
    elseif(is_post_type_archive('tribe_events')){
        $args = array(
            'taxonomy' => 'tribe_events_cat',
            'hide_empty' => 0,
        );
    }
    elseif(is_post_type_archive('article')){
        $args = array(
            'taxonomy' => $taxonomies[0],
            'hide_empty' => 0,
        );
    }
    elseif(is_post_type_archive('media_post')){
        $args = array(
            'taxonomy' => $taxonomies[0],
            'hide_empty' => 0,
        );
    }
    elseif(is_tax()){
        $args = array(
            'taxonomy' => $taxonomy,
            'hide_empty' => 0,
        );
    }
    else {
        $args = array(
            'hide_empty' => 0,
        );
    }
?>
<?php
$current_cat = get_queried_object();
$current_cat_slug = $current_cat->slug;
$post_type = get_post_type( $post->ID );
?>
<?php $categories = get_categories($args); ?>
<div class="blog-filter">
    <nav class="navbar navbar-expand-lg navbar-light navbar-category">
        <button class="navbar-toggler hamburger hamburger--slider" type="button" data-toggle="collapse" data-target="#navbarCategory" aria-controls="navbarCategory" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
        </button>
        <ul class="nav nav-pills nav-category collapse navbar-collapse" id="navbarCategory">
            <li class="nav-item">
                <a class="nav-link">State</a>
            </li>
            <li class="nav-item">
                <a href="<?php echo $archive_link; ?>" class="nav-link <?php echo empty($current_cat_slug) ? 'active':''?>">All</a>
            </li>
            <?php foreach($categories as $category): ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo $category->slug == $current_cat_slug ? 'active':''; ?>" href="<?php echo get_category_link($category->term_id)?>">
                        <?php echo $category->name;?>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </nav>
    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
        <input type="search" class="search-field"
               placeholder="<?php echo esc_attr_x( 'Search News…', 'placeholder' ) ?>"
               value="<?php echo get_search_query() ?>" name="s"
               title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
        <span class="search-trigger" href=""><i class="icon-close"></i></span>
    </form>
    <div class="search-trigger"><i class="icon-searching-magnifying-glass"></i></div>
</div>