<?php
if (post_password_required()) {
    return;
}
?>
<?php if (have_comments() || comments_open()):?>
<section id="comments" class="comments">
    <?php if (have_comments()) : ?>
        <h2><?php printf(_nx('One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'sage'), number_format_i18n(get_comments_number()), '<span>' . get_the_title() . '</span>'); ?></h2>

        <ol class="comment-list">
            <?php wp_list_comments(['style' => 'ol', 'short_ping' => true]); ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav>
                <ul class="pager">
                    <?php if (get_previous_comments_link()) : ?>
                        <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
                    <?php endif; ?>
                    <?php if (get_next_comments_link()) : ?>
                        <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
                    <?php endif; ?>
                </ul>
            </nav>
        <?php endif; ?>
    <?php endif; // have_comments() ?>

    <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
        <div class="alert alert-warning">
            <?php _e('Comments are closed.', 'sage'); ?>
        </div>
    <?php endif; ?>
    <?php
        $fields = array(
            'author' => '<div class="form-row"><div class="form-group col-sm-6"><input id="author" class="form-control" placeholder="Name" name="author" type="text" value=""' . esc_attr( $commenter['comment_author'] ) . $aria_req . ' /></div>',
            'email' => '<div class="form-group col-sm-6"><input id="email" class="form-control" name="email" placeholder="Email" type="text" value=""' . esc_attr( $commenter['comment_author_email'] ) . $aria_req . ' /></div></div>',
            'url' => ""
        );
    ?>
    <?php $comment_args = array(
            'title_reply' =>'',
            'class_submit' => 'btn btn-primary',
            'label_submit'      => __( 'Submit Comment' ),
            'comment_notes_before' => __( '' ),
            'comment_field' => '<div class="form-group"><textarea class="form-control" id="comment" name="comment" placeholder="Leave comment" aria-required="true"></textarea></div>',
            'comment_notes_after' => "",
            'fields' => apply_filters('comment_form_default_fields',$fields)
    );
    ?>
    <?php comment_form($comment_args); ?>
</section>
<?php endif;?>