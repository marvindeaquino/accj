<?php

if( have_rows('section_blocks') ):

    while ( have_rows('section_blocks') ) : the_row();

        if( get_row_layout() == 'master_banner' ):

            get_template_part('templates/flex', 'master-banner');

        elseif( get_row_layout() == 'one_column_block' ):

            get_template_part('templates/flex', 'one-column');

        elseif( get_row_layout() == 'two_column_block_fluid' ):

            get_template_part('templates/flex', 'two-column-fluid');

        elseif( get_row_layout() == 'upcoming_events_block' ):

            get_template_part('templates/flex', 'upcoming-events');

        elseif( get_row_layout() == 'recent_news_block' ):

            get_template_part('templates/flex', 'recent-news');

        elseif( get_row_layout() == 'two_column_block' ):

            get_template_part('templates/flex', 'two-column');

        elseif( get_row_layout() == 'form_and_list' ):

            get_template_part('templates/flex', 'form-and-list');

        elseif( get_row_layout() == 'page_single' ):

            get_template_part('templates/flex', 'page-single');

        elseif( get_row_layout() == 'card_tiles' ):

            get_template_part('templates/flex', 'card-tiles');
        endif;

    endwhile;

endif;
