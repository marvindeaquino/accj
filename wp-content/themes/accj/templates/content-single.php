<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/page', 'header'); ?>
    <div class="section-post-single">
        <div class="container">
            <article <?php post_class(); ?>>
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>

                    <div class="meta-blog">
                        <?php if (!is_singular('resources')):?>
                            <?php get_template_part('templates/entry-meta'); ?>
                            <p class="ml-auto">Share</p>
                            <?php echo do_shortcode('[fny id="1"]')?>
                        <?php endif; ?>
                    </div>

                </header>
                <div class="entry-content">
                    <div class="single-post-featured-img">
                        <?php the_post_thumbnail('full',['class'=>'img-fluid']); ?>
                    </div>
                    <?php the_content(); ?>
                </div>
                <footer>
                    <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
                </footer>
                <?php comments_template('/templates/comments.php'); ?>
            </article>
<?php endwhile; ?>
