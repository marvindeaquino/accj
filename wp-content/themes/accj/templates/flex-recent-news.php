<!--Recent News Section-->
<div class="section section-default section-recent-news secondary-bg">
    <div class="container">
        <div class="text-center">
            <h2>Recent News</h2>
            <h3>Guilding Words for Living</h3>
        </div>

        <div class="card-deck card-deck-news">
            <?php

            // the query
            $args = array(
                'post_type' => 'news',
                'order' => 'DESC',
                'orderby' => 'date',
                'posts_per_page' => 3,
            );
            $the_query = new WP_Query( $args ); ?>
            <?php if ( $the_query->have_posts() ) : ?>

                <!-- pagination here -->

                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="card">
                        <?php //if (!empty(get_the_post_thumbnail())):?>
                            <?php //the_post_thumbnail('medium', array('class' => 'card-img-top')); ?>
                        <?php //else: ?>
                            <!-- <img width="281" height="225" src="<?php //echo get_template_directory_uri();?>/dist/images/subpage/blog-img-0.jpg"  class="card-img-top" alt=""> -->
                        <?php //endif;?>
                        <div class="card-body">
                            <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
                            <?php get_template_part('templates/entry-meta'); ?>
                            <?php echo get_excerpt(95); ?>
                        </div>
                        <div class="card-footer text-center">
                            <a href="<?php the_permalink()?>" class="btn btn-link btn-read-more">Read more</a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--End of Recent News Section-->
