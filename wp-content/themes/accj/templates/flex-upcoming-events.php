<?php $upcoming_events_block = get_sub_field('upcoming_events_block');?>
<!--Upcoming Event Section-->
<div class="section section-default section-upcoming-events">
    <div class="container">
        <div class="text-center">
            <?php echo $upcoming_events_block['content']?>
        </div>
        <div class="event-list">
            <?php

            // the query
            $args = array(
                'post_type' => 'events',
                'posts_per_page' => 3,
            );
            $the_query = new WP_Query( $args ); ?>
            <?php if ( $the_query->have_posts() ) : ?>

                <!-- pagination here -->

                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <li class="event-item">
                        <div class="event-meta">
                            <div class="day"><?php echo get_the_date('d'); ?></div>
                            <div>
                                <div class="month"><?php echo get_the_date('F'); ?></div>
                                <div class="week"><?php echo get_the_date('l'); ?></div>
                            </div>

                        </div>
                        <div class="quick-details">
                            <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
                            <span><?php echo get_the_date( 'j F Y @ g:i a'); ?></span>
                            <p><?php echo get_excerpt(95); ?></p>
                        </div>
                        <div class="more-details">
                            <a href="<?php the_permalink()?>" class="btn btn-outline-primary">Find out more</a>
                        </div>
                    </li>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>

        <div class="text-center">
            <?php if (!empty($upcoming_events_block['link'])): ?>
                <a href="<?php echo $upcoming_events_block['link']['url']; ?>" <?php echo $upcoming_events_block['link']['target'] ? 'target="'.$upcoming_events_block['link']['target'].'"':''; ?> class="btn btn-primary <?php echo $upcoming_events_block['button_type']['value']; ?>">
                    <?php echo !empty($upcoming_events_block['link']['title']) ? $upcoming_events_block['link']['title'] : $upcoming_events_block['button_type']['label']; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--End of Upcoming Event Section-->
