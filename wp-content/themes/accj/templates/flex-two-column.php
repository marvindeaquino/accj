<?php $two_column_blocks = get_sub_field('two_column_block'); ?>
<!--Section About Jewish Community -->
<?php if ($two_column_blocks):?>
    <div class="section section-default section-community-about">
        <div class="container">
            <?php foreach ($two_column_blocks as $two_column_block):?>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="community-about-details">
                            <?php echo $two_column_block['two_column_master_banner']['content']?>
                            <?php if (!empty($two_column_block['two_column_master_banner']['link'])): ?>
                                <a href="<?php echo $two_column_block['two_column_master_banner']['link']['url']?>" <?php echo $two_column_block['two_column_master_banner']['link']['target'] ? 'target="'.$two_column_block['two_column_master_banner']['link']['target'].'"':''; ?> class="btn btn-primary <?php echo $two_column_block['two_column_master_banner']['button_type']['value']; ?>">
                                    <?php echo !empty($two_column_block['two_column_master_banner']['link']['title']) ? $two_column_block['two_column_master_banner']['link']['title'] : $two_column_block['two_column_master_banner']['button_type']['label']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="community-about-img">
                            <img class="img-fluid" src="<?php echo $two_column_block['two_column_master_banner']['background_image']['url'];?>" alt="<?php echo $two_column_block['two_column_master_banner']['background_image']['alt'];?>">
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>

<?php endif;?>

<!--End Section About Jewish Community -->