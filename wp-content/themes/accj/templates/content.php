<div class="card">
    <?php /*if (!empty(get_the_post_thumbnail()) && !is_post_type_archive('news') && !is_post_type_archive('events')):*/?><!--
        <?php /*the_post_thumbnail('medium', array('class' => 'card-img-top')); */?>
    <?php /*else: */?>
        <?php /*if (!is_post_type_archive('news') && !is_post_type_archive('events')): */?>
            <img width="281" height="225" src="<?php /*echo get_template_directory_uri();*/?>/dist/images/subpage/blog-img-0.jpg"  class="card-img-top" alt="">
        <?php /*endif; */?>
    --><?php /*endif;*/?>

    <div class="card-body">
        <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
        <?php if (!is_post_type_archive('events')): ?>
        <?php get_template_part('templates/entry-meta'); ?>
        <?php endif; ?>
        <?php echo get_excerpt(95); ?>
    </div>
    <div class="card-footer text-center">
        <a href="<?php the_permalink()?>" class="btn btn-link btn-read-more">Read more</a>
    </div>
</div>