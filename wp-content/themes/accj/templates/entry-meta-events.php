<p class="card-text"><time class="card-text-meta updated" datetime="<?php echo tribe_get_start_date($post, true, 'c'); ?>"><?php echo tribe_get_start_date($post, true, 'j F Y @ g:i a'); ?></time></p>

<?php if(is_singular()): ?>
    <?php $category = get_the_category(); ?>
    <ul class="list-inline list-style-dot">
        <li class="list-inline-item"><?php echo $category[0]->name; ?></li>
        <li class="list-inline-item"><?= get_the_author(); ?></li>
    </ul>
<?php endif; ?>

