<?php $upcoming_events_block = get_sub_field('upcoming_events_block');?>
<!--Upcoming Event Section-->
<div class="section section-default section-upcoming-events">
    <div class="container">
        <div class="text-center">
            <?php echo $upcoming_events_block['content']?>
        </div>

        <ul class="event-list">

            <?php
            // Ensure the global $post variable is in scope
            global $post;

            // Retrieve the next 5 upcoming events
            $events = tribe_get_events( array(
                'posts_per_page' => 3,
                'start_date' => date( 'Y-m-d H:i:s' )
            ) );

            // Loop through the events: set up each one as
            // the current post then use template tags to
            // display the title and content
            foreach ( $events as $post ):
                setup_postdata($post);
                // This time, let's throw in an event-specific
                // template tag to show the date after the title!
                ?>
                <li class="event-item">
                    <div class="event-meta">
                        <div class="day"><?php echo tribe_get_start_date($post, true, 'd'); ?></div>
                        <div>
                            <div class="month"><?php echo tribe_get_start_date($post, true, 'F'); ?></div>
                            <div class="week"><?php echo tribe_get_start_date($post, true, 'l'); ?></div>
                        </div>

                    </div>
                    <div class="quick-details">
                        <h3><?php the_title(); ?></h3>
                        <span><?php echo tribe_get_start_date($post, true, 'j F Y @ g:i a'); ?></span>
                        <p><?php echo get_excerpt(85); ?></p>
                    </div>
                    <div class="more-details">
                        <a href="<?php the_permalink()?>" class="btn btn-outline-primary">Find out more</a>
                    </div>
                </li>
            <?php endforeach;?>
            <?php wp_reset_postdata(); ?>
        </ul>
        <div class="text-center">
            <?php if (!empty($upcoming_events_block['link'])): ?>
                <a href="<?php echo $upcoming_events_block['link']['url']; ?>" <?php echo $upcoming_events_block['link']['target'] ? 'target="'.$upcoming_events_block['link']['target'].'"':''; ?> class="btn btn-primary <?php echo $upcoming_events_block['button_type']['value']; ?>">
                    <?php echo !empty($upcoming_events_block['link']['title']) ? $upcoming_events_block['link']['title'] : $upcoming_events_block['button_type']['label']; ?>
                </a>
            <?php endif; ?>
        </div>

    </div>
</div>
<!--End of Upcoming Event Section-->