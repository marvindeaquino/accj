<div class="col-md-6 col-lg-6">
<div class="card">
    <?php /*if (!empty(get_the_post_thumbnail())):*/?><!--
        <?php /*the_post_thumbnail('large', array('class' => 'card-img-top')); */?>
    <?php /*else: */?>
        <img width="528" height="316" src="<?php /*echo get_template_directory_uri();*/?>/dist/images/subpage/blog-img-0.jpg"  class="card-img-top" alt="">
    --><?php /*endif;*/?>

    <div class="card-body">
        <a class="title-link" href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
        <?php $product = get_field('product');?>
        <?php echo $product['price']?'<p>'.$product['price'].'</p>':'';?>
        <p><?php echo get_excerpt(95); ?></p>
    </div>
    <div class="card-footer text-center">
        <a href="<?php the_permalink()?>" class="btn btn-primary btn-view">View</a>
    </div>
</div>
</div>