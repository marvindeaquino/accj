<?php $archive = get_field('archive','option');?>
<?php $archive_type = $archive['type']; ?>
<?php
    $post_type = get_post_type();
    if ( $post_type ) {
        $post_type_data = get_post_type_object( $post_type );
        $post_type_slug = $post_type_data->rewrite['slug'];
    }
?>